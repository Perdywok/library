﻿using LibraryApp.DAL.Entities.Enums;
using System.Collections.Generic;

namespace LibraryApp.DAL.Entities
{
    public class Book
    {
        public int BookId { get; set; }

        public string BookName { get; set; }

        public int Pages { get; set; }

        public string Publisher { get; set; }

        public Genre Genre { get; set; }

        public virtual ICollection<Author> Authors { get; set; }

    }
}

