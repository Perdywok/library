﻿using System.Collections.Generic;

namespace LibraryApp.DAL.Entities
{
    public class Brochure
    {
        public int BrochureId { get; set; }

        public string BrochureName { get; set; }

        public int Pages { get; set; }

        public string Publisher { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
    }
}
