﻿namespace LibraryApp.DAL.Entities.Enums
{
    public enum Genre
    {
        Comedy = 0,

        Drama = 1,

        Horror = 2,

        Realism = 3,

        Romance = 4,

        Satire = 5,

        Tragedy = 6,

        Tragicomedy = 7,

        Fantasy = 8,

        Mythology = 9,

        Adventure = 10,
    }
}
