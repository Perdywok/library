﻿namespace LibraryApp.DAL.Entities.Enums
{
    public enum Subscription
    {
        NoSubscription = 0,

        Month = 1,

        ThreeMonth = 2,

        Year = 3
    }
}
