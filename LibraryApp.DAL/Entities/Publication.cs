﻿using LibraryApp.DAL.Entities.Enums;
using System;
using System.Collections.Generic;

namespace LibraryApp.DAL.Entities
{
    public class Publication
    {
        public int PublicationId { get; set; }

        public string PublicationName { get; set; }

        public int Pages { get; set; }

        public string Publisher { get; set; }

        public DateTime PublicationDate { get; set; }

        public Subscription Subscription { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
    }
}
