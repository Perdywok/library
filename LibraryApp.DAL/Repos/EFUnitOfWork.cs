﻿using LibraryApp.DAL.Entities;
using LibraryApp.DAL.Interfaces;
using System;

namespace LibraryApp.DAL.Repos
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private Library db;
        private EFGenericRepository<Book> bookRepository;
        private EFGenericRepository<Brochure> brochureRepository;
        private EFGenericRepository<Publication> publicationRepository;
        private EFGenericRepository<Author> authorRepository;

        public EFUnitOfWork(string connectionString)
        {
           db = new Library(connectionString);
        }
        public IGenericRepository<Book> Books
        {
            get
            {
                if (bookRepository == null)
                    bookRepository = new EFGenericRepository<Book>(db);
                return bookRepository;
            }
        }
        public IGenericRepository<Brochure> Brochures
        {
            get
            {
                if (brochureRepository == null)
                    brochureRepository = new EFGenericRepository<Brochure>(db);
                return brochureRepository;
            }
        }
        public IGenericRepository<Publication> Publications
        {
            get
            {
                if (publicationRepository == null)
                    publicationRepository = new EFGenericRepository<Publication>(db);
                return publicationRepository;
            }
        }

        public IGenericRepository<Author> Authors
        {
            get
            {
                if (authorRepository == null)
                    authorRepository = new EFGenericRepository<Author>(db);
                return authorRepository;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
