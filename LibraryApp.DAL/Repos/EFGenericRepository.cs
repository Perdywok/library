﻿using LibraryApp.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace LibraryApp.DAL.Repos
{
    public class EFGenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        DbContext _context;
        DbSet<TEntity> _dbSet;

        public EFGenericRepository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public void Create(TEntity item)
        {
            _dbSet.Add(item);
            _context.SaveChanges();
        }
        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public void Update()
        {
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var searchItem = FindOne(id);
            if (searchItem != null)
            {
                _dbSet.Remove(searchItem);
            }
            _context.SaveChanges();
        }

        public TEntity FindOne(int id)
        {
            var searchItem = _dbSet.Find(id);
                
            return searchItem;
        }
    }
}
