﻿using LibraryApp.DAL.Entities;
using System;

namespace LibraryApp.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Book> Books { get; }

        IGenericRepository<Author> Authors { get; }

        IGenericRepository<Brochure> Brochures { get; }

        IGenericRepository<Publication> Publications { get; }

    }
}
