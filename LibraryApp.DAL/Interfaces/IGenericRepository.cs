﻿using System.Collections.Generic;

namespace LibraryApp.DAL.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        void Update();

        void Create(TEntity item);

        void Delete(int id);

        TEntity FindOne(int id);
    }
}
