﻿namespace LibraryApp.DAL
{
    using LibraryApp.DAL.Entities;
    using LibraryApp.DAL.Entities.Enums;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class Library : DbContext
    {
        public Library(string connectionString)
           : base(connectionString)
        {
            Database.SetInitializer(new LibraryDbInitializer());
        }
        public DbSet<Book> Books { get; set; }

        public DbSet<Brochure> Brochures { get; set; }

        public DbSet<Publication> Publications { get; set; }

        public DbSet<Author> Authors { get; set; }
    }
    public class LibraryDbInitializer : CreateDatabaseIfNotExists<Library>
    {
        protected override void Seed(Library db)
        {
            if (db.Authors.Count() == 0)
            {
                Author s1 = new Author { AuthorName = "Pyshkin" };
                Author s2 = new Author { AuthorName = "Leonardo" };
                Author s3 = new Author { AuthorName = "Georgii" };
                Author s4 = new Author { AuthorName = "Loman" };
                db.Authors.Add(s1);
                db.Authors.Add(s2);
                db.Authors.Add(s3);
                db.Authors.Add(s4);
            }

            if (db.Books.Count() == 0)
            {
                Book c1 = new Book
                {
                    BookId = 1,
                    BookName = "Операционные системы",
                    Pages = 250,
                    Genre = Genre.Drama,
                    Authors = new List<Author>() { db.Authors.FirstOrDefault() }
                };
                Book c2 = new Book
                {
                    BookId = 2,
                    BookName = "Алгоритмы и структуры данных",
                    Pages = 250,
                    Genre = Genre.Drama,
                };

                db.Books.Add(c1);
                db.Books.Add(c2);
            }

            if (db.Brochures.Count() == 0)
            {
                Brochure b1 = new Brochure
                {
                    BrochureId = 1,
                    BrochureName = "FirstBrochure",
                    Pages = 42,
                    Publisher = "SomeGuy",
                };

                Brochure b2 = new Brochure
                {
                    BrochureId = 2,
                    BrochureName = "SecondBrochure",
                    Pages = 39,
                    Publisher = "AnotherGuy",
                    Authors = new List<Author>() { db.Authors.FirstOrDefault() }
                };

                db.Brochures.Add(b1);
                db.Brochures.Add(b2);
            }

            if (db.Publications.Count() == 0)
            {
                Publication p1 = new Publication
                {
                    PublicationId = 1,
                    PublicationName = "FirstPublication",
                    Pages = 25,
                    Publisher = "SomeGuy",
                    Subscription = Subscription.NoSubscription,
                    PublicationDate = DateTime.Now,
                    Authors = new List<Author>() { db.Authors.FirstOrDefault() }
                };

                Publication p2 = new Publication
                {
                    PublicationId = 2,
                    PublicationName = "SecondPublication",
                    Pages = 28,
                    Publisher = "SomeGuy",
                    Subscription = Subscription.Month,
                    PublicationDate = DateTime.Now,
                };

                db.Publications.Add(p1);
                db.Publications.Add(p2);
            }

            base.Seed(db);
        }
    }

}