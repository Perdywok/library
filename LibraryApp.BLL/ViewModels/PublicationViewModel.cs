﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryApp.BLL.ViewModels
{
    public class PublicationViewModel
    {
        public PublicationViewModel()
        {
            Authors = new List<AuthorViewModel>();
        }
        public int PublicationId { get; set; }

        [Display(Name = "Publication Name")]
        public string PublicationName { get; set; }

        public int Pages { get; set; }

        public string Publisher { get; set; }

        [Display(Name = "Publication Date")]
        public DateTime PublicationDate { get; set; }

        public SubscriptionViewModel Subscription { get; set; }

        [UIHint("AuthorsEditor")]
        public virtual List<AuthorViewModel> Authors { get; set; }
    }
}
