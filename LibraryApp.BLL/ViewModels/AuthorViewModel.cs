﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryApp.BLL.ViewModels
{
    public class AuthorViewModel
    {
        public int AuthorId { get; set; }

        [Display(Name = "Author Name")]
        [MaxLength(100, ErrorMessage = "Author Name must be 100 characters or less")]
        public string AuthorName { get; set; }
        public static List<AuthorViewModel> GetAllAuthors { get; set; }

    }
}