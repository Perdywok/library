﻿using LibraryApp.DAL.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.BLL.ViewModels
{
    public class GenreViewModel
    {
        public int GenreId { get; set; }
        public string GenreName { get { return Enum.GetName(typeof(Genre), GenreId); } }

        public static List<GenreViewModel> GetAllGenres
        {
            get
            {
                var allGenres = Enum.GetValues(typeof(Genre)).Cast<Genre>().Select(v => new GenreViewModel
                {
                    GenreId = ((int)v)
                }).ToList();
                return allGenres;
            }
        }
    }
}