﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryApp.BLL.ViewModels
{
    public class BookViewModel
    {
        public BookViewModel()
        {
            Authors = new List<AuthorViewModel>();
        }

        [ScaffoldColumn(false)]
        public int BookId { get; set; }

        [Display(Name = "Book Name")]
        [MaxLength(100, ErrorMessage = "Book Name must be 100 characters or less"), MinLength(5)]
        public string BookName { get; set; }
        public int Pages { get; set; }

        public string Publisher { get; set; }

        public GenreViewModel Genre { get; set; }

        [UIHint("AuthorsEditor")]
        public virtual List<AuthorViewModel> Authors { get; set; }    
    }
}