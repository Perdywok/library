﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryApp.BLL.ViewModels
{
    public class PaperProductViewModel
    {
        public PaperProductViewModel()
        {
            Authors = new List<AuthorViewModel>();
        }
        public int PaperProductId { get; set; }

        [Display(Name = "Book Name")]
        [MaxLength(100, ErrorMessage = "Book Name must be 100 characters or less"), MinLength(5)]
        public string PaperProductName { get; set; }
        public int Pages { get; set; }

        public string Type { get; set; }

        public string Publisher { get; set; }

        [UIHint("AuthorsEditor")]
        public virtual List<AuthorViewModel> Authors { get; set; }
    }

}
