﻿using LibraryApp.DAL.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.BLL.ViewModels
{
    public class SubscriptionViewModel
    {
        public int SubscriptionId { get; set; }
        public string SubscriptionName { get { return Enum.GetName(typeof(Subscription), SubscriptionId); } }

        public static List<SubscriptionViewModel> GetAllSubscriptions
        {
            get
            {
                return Enum.GetValues(typeof(Subscription)).Cast<Subscription>().Select(s => new SubscriptionViewModel
                {
                    SubscriptionId = ((int)s)
                }).ToList();
            }
        }
    }
}

