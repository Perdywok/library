﻿using AutoMapper;
using LibraryApp.BLL.Interfaces;
using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.Entities.Enums;
using LibraryApp.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.BLL.Services
{
    public class PublicationService /*: IService<PublicationViewModel>*/
    {
        IUnitOfWork db { get; set; }
        public PublicationService(IUnitOfWork uow)
        {
            db = uow;
        }

        public IEnumerable<PublicationViewModel> GetAllObjects()
        {
            var publications = db.Publications.GetAll().Select(e => Mapper.Map<Publication, PublicationViewModel>(e)).ToList();

            return publications;
        }

        public IEnumerable<AuthorViewModel> GetAuthors()
        {
            var authors = db.Authors.GetAll().Select(e => Mapper.Map<Author, AuthorViewModel>(e)).ToList();

            return authors;
        }

        public void UpdateObject(PublicationViewModel viewModel)
        {
            var parsePublication = Mapper.Map<PublicationViewModel, Publication>(viewModel);
            Publication viewModelPublication = parsePublication;
            var publication = db.Publications.FindOne(viewModel.PublicationId);
            publication.Pages = viewModelPublication.Pages;
            publication.PublicationDate = viewModelPublication.PublicationDate;
            publication.PublicationName = viewModelPublication.PublicationName;
            publication.Publisher = viewModelPublication.Publisher;
            publication.Subscription = viewModelPublication.Subscription;
            publication.Authors = viewModelPublication.Authors;
            db.Publications.Update();
        }

        public int CreateObject(PublicationViewModel viewModel)
        {
            var parsePublication = Mapper.Map<PublicationViewModel, Publication>(viewModel);
            Publication viewModelPublication = parsePublication;
            Publication publication = viewModelPublication;
            db.Publications.Create(publication);
            db.Publications.Update();
            return publication.PublicationId;
        }

        public void DeleteObject(PublicationViewModel viewModel)
        {
            var publication = db.Publications.FindOne(viewModel.PublicationId);
            if (publication != null)
            {
                db.Publications.Delete(publication.PublicationId);
            }
            db.Publications.Update();
        }
        public IEnumerable<SubscriptionViewModel> GetAllSubscriptions()
        {
            return Enum.GetValues(typeof(Subscription)).Cast<Subscription>().Select(s => new SubscriptionViewModel
            {
                SubscriptionId = ((int)s)
            });
            //var allSubscriptions = Enum.GetValues(typeof(Subscription)).Cast<Subscription>().Select(v => Mapper.Map<Subscription, SubscriptionViewModel>(v));
            //return allSubscriptions;
        }

    }
}
