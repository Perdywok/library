﻿using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.Interfaces;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.BLL.Services
{
    public class BrochureService /*: IService<BrochureViewModel>*/
    {
        IUnitOfWork db { get; set; }
        public BrochureService(IUnitOfWork uow)
        {
            db = uow;
        }

        public IEnumerable<BrochureViewModel> GetAllObjects()
        {
            var brochures = db.Brochures.GetAll().Select(e => Mapper.Map<Brochure, BrochureViewModel>(e)).ToList();

            return brochures;
        }

        public IEnumerable<AuthorViewModel> GetAuthors()
        {
            var authors = db.Authors.GetAll().Select(e => Mapper.Map<Author, AuthorViewModel>(e)).ToList();

            return authors;
        }

        public void UpdateObject(BrochureViewModel viewModel)
        {
            var parseBrochure = Mapper.Map<BrochureViewModel, Brochure>(viewModel);
            Brochure viewModelBrochure = parseBrochure;
            var brochure = db.Brochures.FindOne(viewModel.BrochureId);
            brochure.BrochureId = viewModelBrochure.BrochureId;
            brochure.BrochureName = viewModelBrochure.BrochureName;
            brochure.Pages = viewModelBrochure.Pages;
            brochure.Publisher = viewModelBrochure.Publisher;
            brochure.Authors = viewModelBrochure.Authors;
            db.Brochures.Update();
        }

        public int CreateObject(BrochureViewModel viewModel)
        {
            var parseBrochure = Mapper.Map<BrochureViewModel, Brochure>(viewModel);
            Brochure viewModelBrochure = parseBrochure;
            db.Brochures.Create(viewModelBrochure);
            db.Brochures.Update();
            return viewModelBrochure.BrochureId;
        }

        public void DeleteObject(BrochureViewModel viewModel)
        {
            var brochure = db.Brochures.FindOne(viewModel.BrochureId);
            if (brochure != null)
            {
                db.Brochures.Delete(brochure.BrochureId);
            }
            db.Brochures.Update();
        }

    }
}
