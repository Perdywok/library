﻿using AutoMapper;
using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.Interfaces;
using LibraryApp.DAL.Repos;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.BLL.Services
{
    public class PaperProductService
    {
        IUnitOfWork db { get; set; }

        public PaperProductService()
        {
            db = new EFUnitOfWork("DefaultConnection");
        }

        public IEnumerable<PaperProductViewModel> GetAllObjects()
        {

            List<PaperProductViewModel> paperProducts = new List<PaperProductViewModel>();

            var books = db.Books.GetAll().Select(e => Mapper.Map<Book, PaperProductViewModel>(e)).ToList();
            var publications = db.Publications.GetAll().Select(e => Mapper.Map<Publication, PaperProductViewModel>(e)).ToList();
            var brochures = db.Brochures.GetAll().Select(e => Mapper.Map<Brochure, PaperProductViewModel>(e)).ToList();
           
            paperProducts.AddRange(books);
            paperProducts.AddRange(publications);
            paperProducts.AddRange(brochures);
            return paperProducts;
        }
    }
}
