﻿using AutoMapper;
using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.Entities.Enums;
using LibraryApp.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.BLL.Services
{
    public class BookService 
    {
        IUnitOfWork db { get; set; }
        public BookService(IUnitOfWork uow)
        {
            db = uow;
        }

        public IEnumerable<BookViewModel> GetAllObjects()
        {
            var books = db.Books.GetAll().Select(e => Mapper.Map<Book, BookViewModel>(e)).ToList();

            return books;
        }

        public IEnumerable<AuthorViewModel> GetAuthors()
        {
            var authors = db.Authors.GetAll().Select(e => Mapper.Map<Author, AuthorViewModel>(e)).ToList();

            return authors;
        }

        public void UpdateObject(BookViewModel viewModel)
        {
            var parseBook = Mapper.Map<BookViewModel, Book>(viewModel);
            Book viewModelBook = parseBook;
            var book = db.Books.FindOne(viewModel.BookId);
            book.BookId = viewModelBook.BookId;
            book.BookName = viewModelBook.BookName;
            book.Genre = viewModelBook.Genre;
            book.Pages = viewModelBook.Pages;
            book.Publisher = viewModelBook.Publisher;
            book.Authors = viewModelBook.Authors;

            db.Books.Update();
        }

        public int CreateObject(BookViewModel viewModel)
        {
            var parseBook = Mapper.Map<BookViewModel, Book>(viewModel);
            Book viewModelBook = parseBook;
            db.Books.Create(viewModelBook);
            db.Books.Update();
            return viewModelBook.BookId;
        }

        public void DeleteObject(BookViewModel viewModel)
        {
            Book book = db.Books.FindOne(viewModel.BookId);
            if (book != null)
            {
                db.Books.Delete(book.BookId);
            }
            db.Books.Update();
        }

        public IEnumerable<GenreViewModel> GetAllGenres()
        {
            var allGenres = Enum.GetValues(typeof(Genre)).Cast<Genre>().Select(v => new GenreViewModel
            {
                GenreId = ((int)v)
            });
            return allGenres;
        }
    }
}
