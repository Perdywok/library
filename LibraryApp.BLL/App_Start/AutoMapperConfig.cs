﻿using AutoMapper;
using LibraryApp.BLL.Profiles;

namespace LibraryApp.BLL.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                //cfg.AddProfile<GenreProfile>();
                //cfg.AddProfile<SubscriptionProfile>();
                cfg.AddProfile<BookProfile>();
                cfg.AddProfile<PublicationProfile>();
                cfg.AddProfile<BrochureProfile>();
                cfg.AddProfile<AuthorProfile>();
                cfg.AddProfile<PaperProductProfile>();
                

            });
        }
    }
}