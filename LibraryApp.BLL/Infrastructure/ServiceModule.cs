﻿using Ninject.Modules;
using LibraryApp.DAL.Interfaces;
using LibraryApp.DAL.Repos;
using Ninject;

namespace LibraryApp.BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private string connectionString;
        public StandardKernel _kernel;
        IUnitOfWork uow;
        public ServiceModule(string connection)
        {
            connectionString = connection;
            uow = new EFUnitOfWork(connectionString);
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<EFUnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}