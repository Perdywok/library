﻿using AutoMapper;
using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
using System.Linq;

namespace LibraryApp.BLL.Profiles
{
    public class PaperProductProfile : Profile
    {
        public PaperProductProfile()
        {
            CreateMap<Book, PaperProductViewModel>()
                .ForMember(b => b.PaperProductId, opt => opt.MapFrom(b => b.BookId))
                .ForMember(b => b.PaperProductName, opt => opt.MapFrom(b => b.BookName))
                .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
                .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
                .ForMember(b => b.Type, opt => opt.MapFrom(b => typeof(Book).Name))
                .ForMember(b => b.Authors, opt => opt.MapFrom(b => b.Authors.ToList()));

            CreateMap<Brochure, PaperProductViewModel>()
               .ForMember(b => b.PaperProductId, opt => opt.MapFrom(b => b.BrochureId))
               .ForMember(b => b.PaperProductName, opt => opt.MapFrom(b => b.BrochureName))
               .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
               .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
               .ForMember(b => b.Type, opt => opt.MapFrom(b => typeof(Brochure).Name))
               .ForMember(b => b.Authors, opt => opt.MapFrom(b => b.Authors.ToList()));

            CreateMap<Publication, PaperProductViewModel>()
               .ForMember(p => p.PaperProductId, opt => opt.MapFrom(p => p.PublicationId))
               .ForMember(p => p.PaperProductName, opt => opt.MapFrom(p => p.PublicationName))
               .ForMember(p => p.Pages, opt => opt.MapFrom(p => p.Pages))
               .ForMember(p => p.Publisher, opt => opt.MapFrom(p => p.Publisher))
               .ForMember(b => b.Type, opt => opt.MapFrom(b => typeof(Publication).Name))
               .ForMember(p => p.Authors, opt => opt.MapFrom(p => p.Authors.ToList()));
        }
    }
}
