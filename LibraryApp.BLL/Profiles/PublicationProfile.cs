﻿using AutoMapper;
using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
using System.Linq;

namespace LibraryApp.BLL.Profiles
{
    public class PublicationProfile : Profile
    {
        public PublicationProfile()
        {
            CreateMap<Publication, PublicationViewModel>()
            .ForMember(p => p.PublicationId, opt => opt.MapFrom(p => p.PublicationId))
            .ForMember(p => p.PublicationName, opt => opt.MapFrom(p => p.PublicationName))
            .ForMember(p => p.Pages, opt => opt.MapFrom(p => p.Pages))
            .ForMember(p => p.Publisher, opt => opt.MapFrom(p => p.Publisher))
            .ForMember(p => p.PublicationDate, opt => opt.MapFrom(p => p.PublicationDate))
            .ForMember(p => p.Subscription, opt => opt.MapFrom(p => new SubscriptionViewModel
            {
                SubscriptionId = ((int)p.Subscription)
            }))
            .ForMember(p => p.Authors, opt => opt.MapFrom(p => p.Authors.ToList()));

            CreateMap<PublicationViewModel, Publication>()
                .ForMember(p => p.PublicationId, opt => opt.MapFrom(p => p.PublicationId))
                .ForMember(p => p.PublicationName, opt => opt.MapFrom(p => p.PublicationName))
                .ForMember(p => p.Pages, opt => opt.MapFrom(p => p.Pages))
                .ForMember(p => p.Publisher, opt => opt.MapFrom(p => p.Publisher))
                .ForMember(p => p.PublicationDate, opt => opt.MapFrom(p => p.PublicationDate))
                .ForMember(p => p.Subscription, opt => opt.MapFrom(p => p.Subscription.SubscriptionId))
                .ForMember(p => p.Authors, opt => opt.MapFrom(p => p.Authors));
        }
    }
}
