﻿using AutoMapper;
using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
using System.Linq;

namespace LibraryApp.BLL.Profiles
{
    public class BrochureProfile : Profile
    {
        public BrochureProfile()
        {
            CreateMap<Brochure, BrochureViewModel>()
             .ForMember(b => b.BrochureId, opt => opt.MapFrom(b => b.BrochureId))
             .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
             .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
             .ForMember(b => b.BrochureName, opt => opt.MapFrom(b => b.BrochureName))
             .ForMember(b => b.Authors, opt => opt.MapFrom(b => b.Authors.ToList()));

            CreateMap<BrochureViewModel, Brochure>()
             .ForMember(b => b.BrochureId, opt => opt.MapFrom(b => b.BrochureId))
            .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
            .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
            .ForMember(b => b.BrochureName, opt => opt.MapFrom(b => b.BrochureName))
            .ForMember(b => b.Authors, opt => opt.MapFrom(b => b.Authors));
        }
    }
}
