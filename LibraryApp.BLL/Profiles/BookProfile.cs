﻿using AutoMapper;
using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
using System.Linq;

namespace LibraryApp.BLL.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, BookViewModel>()
                .ForMember(b => b.BookId, opt => opt.MapFrom(b => b.BookId))
                .ForMember(b => b.BookName, opt => opt.MapFrom(b => b.BookName))
                .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
                .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
                .ForMember(b => b.Genre, opt => opt.MapFrom(b => new GenreViewModel
                {
                    GenreId = ((int)b.Genre)
                }))
                .ForMember(b => b.Authors, opt => opt.MapFrom(b => b.Authors.ToList()));

            CreateMap<BookViewModel, Book>()
                .ForMember(b => b.BookId, opt => opt.MapFrom(b => b.BookId))
                .ForMember(b => b.BookName, opt => opt.MapFrom(b => b.BookName))
                .ForMember(b => b.Pages, opt => opt.MapFrom(b => b.Pages))
                .ForMember(b => b.Publisher, opt => opt.MapFrom(b => b.Publisher))
                .ForMember(b => b.Genre, opt => opt.MapFrom(b => b.Genre.GenreId))
                .ForMember(b => b.Authors, opt => opt.MapFrom(b => b.Authors));
        }
    }
}
