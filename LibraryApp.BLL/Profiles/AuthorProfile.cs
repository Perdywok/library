﻿using AutoMapper;
using LibraryApp.BLL.ViewModels;
using LibraryApp.DAL.Entities;
namespace LibraryApp.BLL.Profiles
{
    public class AuthorProfile : Profile
    {
        public AuthorProfile()
        {
            CreateMap<Author, AuthorViewModel>()
               .ForMember(a => a.AuthorId, opt => opt.MapFrom(a => a.AuthorId))
               .ForMember(a => a.AuthorName, opt => opt.MapFrom(a => a.AuthorName));

            CreateMap<AuthorViewModel, Author>()
          .ForMember(a => a.AuthorId, opt => opt.MapFrom(a => a.AuthorId))
          .ForMember(a => a.AuthorName, opt => opt.MapFrom(a => a.AuthorName));
        }
    }
}
