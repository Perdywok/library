﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LibraryApp.BLL.ViewModels;
using System.Web.Mvc;
using LibraryApp.BLL.Services;
using System.Collections.Generic;

namespace LibraryApp.WEB.Controllers
{
    public class PublicationController : Controller
    {
        private PublicationService publicationService;

        public PublicationController(PublicationService serv)
        {
            publicationService = serv;
        }

        public ActionResult Index()
        {
            if (User.IsInRole("admin"))
            {
                return View("Index", "~/Views/Shared/_AdminLayout.cshtml");
            }
            return View("ViewForUsers");
        }

        public ActionResult Read()
        {
            IEnumerable<PublicationViewModel> publications = publicationService.GetAllObjects();
            return Json(publications);
        }

        [HttpPost]
        public ActionResult Update(PublicationViewModel publication)
        {
            if (ModelState.IsValid)
            {
                publicationService.UpdateObject(publication);
            }
            return Json(new[] { publication }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost]
        public ActionResult Create(PublicationViewModel publication)
        {

            if (ModelState.IsValid)
            {
                publication.PublicationId = publicationService.CreateObject(publication);
            }

            return Json(new[] { publication }.ToDataSourceResult(new DataSourceRequest(), ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Destroy(PublicationViewModel publication)
        {
            if (ModelState.IsValid)
            {
                publicationService.DeleteObject(publication);
            }
            return Json(new[] { publication }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }
        public JsonResult GetAllAuthors()
        {
            IEnumerable<AuthorViewModel> authors = publicationService.GetAuthors();
            return Json(authors, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllGenres()
        {
            IEnumerable<SubscriptionViewModel> genres = publicationService.GetAllSubscriptions();
            return Json(genres, JsonRequestBehavior.AllowGet);
        }
    }
}