﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LibraryApp.BLL.ViewModels;
using System.Web.Mvc;
using LibraryApp.BLL.Services;
using System.Collections.Generic;

namespace LibraryApp.WEB.Controllers
{
    public class BrochureController : Controller
    {
        private BrochureService brochureService;

        public BrochureController(BrochureService serv)
        {
            brochureService = serv;
        }

        public ActionResult Index()
        {
            if (User.IsInRole("admin"))
            {
                return View("Index", "~/Views/Shared/_AdminLayout.cshtml");
            }
            return View("ViewForUsers");
        }

        public ActionResult Read()
        {
            IEnumerable<BrochureViewModel> brochures = brochureService.GetAllObjects();
            return Json(brochures);
        }

        [HttpPost]
        public ActionResult Update(BrochureViewModel brochure)
        {
            if (ModelState.IsValid)
            {
                brochureService.UpdateObject(brochure);
            }

            return Json(new[] { brochure }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost]
        public ActionResult Create(BrochureViewModel brochure)
        {
            if (ModelState.IsValid)
            {
                brochure.BrochureId = brochureService.CreateObject(brochure);
            }

            return Json(new[] { brochure }.ToDataSourceResult(new DataSourceRequest(), ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Destroy(BrochureViewModel brochure)
        {
            if (ModelState.IsValid)
            {
                brochureService.DeleteObject(brochure);
            }
            return Json(new[] { brochure }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }
        public JsonResult GetAllAuthors()
        {
            IEnumerable<AuthorViewModel> authors = brochureService.GetAuthors();
            return Json(authors, JsonRequestBehavior.AllowGet);
        }
    }
}