﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Web.Mvc;
using LibraryApp.BLL.ViewModels;
using LibraryApp.BLL.Services;
using System.Collections.Generic;

namespace LibraryApp.WEB.Controllers
{
    public class GridController : Controller
    {
        private BookService bookService;

        public GridController(BookService serv)
        {
            bookService = serv;
        }

        public ActionResult Index()
        {
            if (User.IsInRole("admin"))
            {
                return View("Index", "~/Views/Shared/_AdminLayout.cshtml");
            }
            return View("ViewForUsers");
        }

        public ActionResult Read()
        {
            IEnumerable<BookViewModel> books = bookService.GetAllObjects();
            return Json(books);
        }

        [HttpPost]
        public ActionResult Update(BookViewModel book)
        {

            if (ModelState.IsValid)
            {
                bookService.UpdateObject(book);
            }

            return Json(new[] { book }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost]
        public ActionResult Create(BookViewModel book)
        {

            if (ModelState.IsValid)
            {
                book.BookId = bookService.CreateObject(book);

            }

            return Json(new[] { book }.ToDataSourceResult(new DataSourceRequest(), ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Destroy(BookViewModel book)
        {
            if (ModelState.IsValid)
            {
                bookService.DeleteObject(book);
            }
            return Json(new[] { book }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }
        public JsonResult GetAllAuthors()
        {
            IEnumerable<AuthorViewModel> authors = bookService.GetAuthors();
            return Json(authors, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllGenres()
        {
            IEnumerable<GenreViewModel> genres = bookService.GetAllGenres();
            return Json(genres, JsonRequestBehavior.AllowGet);
        }
    }
}
