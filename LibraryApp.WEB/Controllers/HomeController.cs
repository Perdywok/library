﻿using System.Collections.Generic;
using System.Web.Mvc;
using LibraryApp.BLL.Services;
using LibraryApp.BLL.ViewModels;

namespace LibraryApp.WEB.Controllers
{
    public class HomeController : Controller
    {
        private PaperProductService paperService;

        public HomeController()
        {
            paperService = new PaperProductService();
        }


        public ActionResult Index()
        {
            if (User.IsInRole("admin"))
            {
                return View("Index", "~/Views/Shared/_AdminLayout.cshtml");
            }
            return View("Index");
        }

        public ActionResult Read()
        {
            IEnumerable<PaperProductViewModel> paperProducts = paperService.GetAllObjects();
            return Json(paperProducts);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}