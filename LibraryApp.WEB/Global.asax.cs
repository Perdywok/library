﻿using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using LibraryApp.BLL.Infrastructure;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LibraryApp.BLL.App_Start;
using System.Data.Entity;
using LibraryApp.WEB.Models;

namespace LibraryApp.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {


            Database.SetInitializer(new AppDbInitializer());          
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            AutoMapperConfig.Initialize();
            // внедрение зависимостей
            NinjectModule registrations = new ServiceModule("DefaultConnection");
            var kernel = new StandardKernel(registrations);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
            
        }
    }
}
