﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LibraryApp.WEB.Startup))]
namespace LibraryApp.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
