// <auto-generated />
namespace LibraryApp.WEB.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class EnumsWereChanged : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EnumsWereChanged));
        
        string IMigrationMetadata.Id
        {
            get { return "201803161101522_EnumsWereChanged"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
