﻿var grid = $("#grid").kendoGrid({
    pageable: true,
    toolbar: ["create"],
    dataSource: {
        pageSize: 5,
        transport: {
            read: {
                url: "/Brochure/Read/",
                type: "POST"
            },
            update: {
                url: "/Brochure/Update/",
                contentType: "application/json",
                type: "POST"
            },
            create: {
                url: "/Brochure/Create/",
                contentType: "application/json",
                type: "POST"
            },
            destroy: {
                url: "/Brochure/Destroy/",
                contentType: "application/json",
                type: "POST"
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            },
        },
        schema: {
            model: {
                id: "BrochureId",
                fields: {
                    BrochureId: { defaultValue: 0, editable: false, validation: { required: false } },
                    Pages: { type: "number" },
                    Publisher: { type: "string" }
                }
            }
        }
    },
    editable: "inline",
    columns: [
        { field: "BrochureName", width: 90, title: "Brochure Name" },
        "Pages",
        "Publisher",
        {
            editor: authorsEditor, field: "Authors", template: $("#authorsTemplate").html(),
        },
        { command: ["edit", "destroy"] }
    ],
    editable: "inline",
    save: onSave
});
function onSave(e) {
    e.sender.one("dataBound", function () {
        e.sender.dataSource.read();
    });
}
function authorsEditor(container, options) {
    $('<input name="Authors">').appendTo(container)
        .kendoMultiSelect({
            dataValueField: "AuthorId",
            dataTextField: "AuthorName",
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/Brochure/GetAllAuthors",
                    }
                }
            }),
        });
}
