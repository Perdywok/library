﻿var grid = $("#grid").kendoGrid({
    pageable: true,
    dataSource: {
        pageSize: 5,
        transport: {
            read: {
                url: "/Brochure/Read/",
                type: "POST"
            }
        },
        schema: {
            model: {
                id: "BrochureId",
                fields: {
                    BrochureId: { defaultValue: 0, editable: false, validation: { required: false } },
                    Authors: {},
                    Pages: { type: "number" },
                    Publisher: { type: "string" },
                }
            }
        }
    },
    editable: "inline",
    columns: [
        { field: "BrochureName", width: 90, title: "Brochure Name" },
        "Pages",
        "Publisher",
        {
            field: "Authors", template: $("#authorsTemplate").html(),
        },
    ],

});
