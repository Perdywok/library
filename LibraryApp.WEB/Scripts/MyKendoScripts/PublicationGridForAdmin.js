﻿        var grid =  $("#grid").kendoGrid({
            pageable: true,
        toolbar: ["create"],
        dataSource: {
            pageSize: 5,
            transport: {
            read: {
                url: "/Publication/Read/",
                    type: "POST"
                },
                update: {
                    url: "/Publication/Update/",
                    contentType: "application/json",
                    type: "POST"
                },
                create: {
                    url: "/Publication/Create/",
                    contentType: "application/json",
                    type: "POST"
                },
                 destroy: {
                     url: "/Publication/Destroy/",
                    contentType: "application/json",
                    type: "POST"
                },
                 parameterMap: function (options) {
                    return kendo.stringify(options);
                },

            },
            schema: {
            model: {
            id: "PublicationId",
                    fields: {
            PublicationId: {defaultValue: 0, editable: false, validation: {required: false } },
                        Pages: {type: "number" },
                        Publisher: {type: "string" },
                        PublicationDate: {type: "date" }
                    }
                }
            }
        },
        editable: "inline",
        columns: [
            {field: "PublicationName", width: 90, title: "Publication Name" },
            "Pages",
            {
            field: "Subscription", title: "Subscription", width: "180px",
                editor: subscriptionDropDownEditor, template: $("#subscriptionsTemplate").html()

            },
            {field: "PublicationDate", type: "date", format: "{0:MM/dd/yyyy}" },

            "Publisher",
            {
            editor: authorsEditor, field: "Authors", template: $("#authorsTemplate").html(),
            },
            {command: ["edit", "destroy"] }
        ],
        editable: "inline",
        save: onSave
    });
    function onSave(e) {
            e.sender.one("dataBound", function () {
                e.sender.dataSource.read();
            });
        }

    function subscriptionDropDownEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataTextField: "SubscriptionName",
                    dataValueField: "SubscriptionId",
                    dataSource: new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: "/Publication/GetAllSubscriptions",
                            }
                        }
                    }),
    });
    }
     function authorsEditor(container, options) {
            $('<input name="Authors">').appendTo(container)
                .kendoMultiSelect({
                    dataValueField: "AuthorId",
                    dataTextField: "AuthorName",
                    dataSource: new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: "/Publication/GetAllAuthors",
                            }
                        }
                    }),
            });
    }