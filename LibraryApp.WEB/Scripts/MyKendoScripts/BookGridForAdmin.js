﻿var grid = $("#grid").kendoGrid({
    pageable: true,
    toolbar: ["create"],
    dataSource: {
        pageSize: 5,
        transport: {
            read: {
                url: "/Grid/Read/",
                type: "POST"
            },
            update: {
                url: "/Grid/Update/",
                contentType: "application/json",
                type: "POST"
            },
            create: {
                url: "/Grid/Create/",
                contentType: "application/json",
                type: "POST"
            },
            destroy: {
                url: "/Grid/Destroy/",
                contentType: "application/json",
                type: "POST"
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            },
        },
        schema: {
            model: {
                id: "BookId",
                fields: {
                    BookId: { defaultValue: 0, editable: false, validation: { required: false } },
                    Pages: { type: "number" },
                    Publisher: { type: "string" }
                }
            }
        }
    },
    editable: "inline",
    columns: [
        { field: "BookName", width: 90, title: "Book Name" },
        "Pages",
        {
            field: "Genre", title: "Genre", width: "180px", editor: genreDropDownEditor, template: $("#genresTemplate").html()

        },
        "Publisher",
        {
            editor: authorsEditor, field: "Authors", template: $("#authorsTemplate").html(),
        },
        { command: ["edit", "destroy"] }
    ],
    editable: "inline",
    save: onSave
});

function onSave(e) {
    e.sender.one("dataBound", function () {
        e.sender.dataSource.read();
    });
}

function genreDropDownEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            autoBind: false,
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/Grid/GetAllGenres",
                    }
                }
            }),
            dataTextField: "GenreName",
            dataValueField: "GenreId",
        });
}
function authorsEditor(container, options) {
    $('<input name="Authors">').appendTo(container)
        .kendoMultiSelect({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/Grid/GetAllAuthors",
                    }
                }
            }),
            dataValueField: "AuthorId",
            dataTextField: "AuthorName",
        });
};