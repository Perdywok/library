﻿
var grid = $("#grid").kendoGrid({
    pageable: true,
    dataSource: {
        pageSize: 5,
        transport: {
            read: {
                url: "/Publication/Read/",
                type: "POST"
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            },

        },
        schema: {
            model: {
                id: "PublicationId",
                fields: {
                    PublicationId: { defaultValue: 0, editable: false, validation: { required: false } },
                    Pages: { type: "number" },
                    Publisher: { type: "string" },
                    PublicationDate: { type: "date" }
                }
            }
        }
    },
    editable: "inline",
    columns: [
        { field: "PublicationName", width: 90, title: "Publication Name" },
        "Pages",
        {
            field: "Subscription", title: "Subscription", width: "180px",
            template: $("#subscriptionsTemplate").html()

        },
        { field: "PublicationDate", type: "date", format: "{0:MM/dd/yyyy}" },

        "Publisher",
        {
            field: "Authors", template: $("#authorsTemplate").html(),
        },
    ],
    editable: "inline",
});
