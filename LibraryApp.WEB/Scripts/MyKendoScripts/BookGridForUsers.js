﻿var grid = $("#grid").kendoGrid({
    pageable: true,
    dataSource: {
        pageSize: 5,
        transport: {
            read: {
                url: "/Grid/Read/",
                type: "POST"
            }

        },
        schema: {
            model: {
                id: "BookId",
                fields: {
                    BookId: { defaultValue: 0, editable: false, validation: { required: false } },
                    Authors: {},
                    Pages: { type: "number" },
                    Publisher: { type: "string" },
                }
            }
        }
    },
    editable: "inline",
    columns: [
        { field: "BookName", width: 90, title: "Book Name" },
        "Pages",
        {
            field: "Genre", title: "Genre", width: "180px", template: "#= Genre.GenreName #",

        },

        "Publisher",
        {
            field: "Authors", template: $("#authorsTemplate").html(),
        },

    ],
});
