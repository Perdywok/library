﻿var grid = $("#grid").kendoGrid({
    pageable: true,
    dataSource: {
        pageSize: 5,
        transport: {
            read: {
                url: "/Home/Read/",
                type: "POST"
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            },
        },
        schema: {
            model: {
                id: "PaperProductId",
                fields: {
                    PaperProductId: { defaultValue: 0, editable: false, validation: { required: false } },
                    Pages: { type: "number" },
                    Publisher: { type: "string" },
                }
            }
        }
    },
    editable: "inline",
    columns: [
        { field: "PaperProductName", width: 90, title: "Name" },
        "Pages",
        "Type",
        "Publisher",
        {
            field: "Authors", template: $("#authorsTemplate").html(),
        },
    ],
});
